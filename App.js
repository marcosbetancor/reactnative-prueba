import React from 'react';
import { FlatList, ActivityIndicator, Text, View, StyleSheet, Image  } from 'react-native';

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true, recetas: []}
  }

  componentDidMount(){
    return fetch('https://jsonplaceholder.typicode.com/photos')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource:responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }


  

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(

      <View style={{flex: 1, paddingTop:20}}>
        <Text>Listado de Peliculas</Text>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) =>
          
          <View style={this.styles.row}>
            <Image style={{width:80 , height: 80}}  source={{uri: item.thumbnailUrl}} />
            <Text>Hola! {item.title}</Text>
          </View>
          }
            keyExtractor={({id}, index) => id.toString()}
        />
      </View>
    );
  }


  styles = StyleSheet.create({
    row: {
      backgroundColor: 'lightblue',
      borderColor: 'black',
      padding: 1,
      flexDirection: 'row',
      alignItems: 'center',

    },
  });
  
}
